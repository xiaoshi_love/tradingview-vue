export default [
    {
        label: "分时",
        resolution: "1s",
        chartType: 3,
        websockSend: "0",
    },
    {
        label: "1分钟",
        resolution: "1",
        websockSend: "1",
    },
    {
        label: "5分钟",
        resolution: "5",
        websockSend: "5",
    },
    {
        label: "15分钟",
        resolution: "15",
        websockSend: "15",
    },
    {
        label: "30分钟",
        resolution: "30",
        websockSend: "30",
    },
    {
        label: "1小时",
        resolution: "60",
        websockSend: "60",
    },
    {
        label: "日线",
        resolution: "1D",
        websockSend: "1D",
    },
    {
        label: "周线",
        resolution: "1W",
        websockSend: "1W",
    },
    {
        label: "月线",
        resolution: "1M",
        websockSend: "1M",
    },
]